(()=>{
    jQuery(document).ready(function($){
        priceSlider();
        durationSlider();
        rateSlider();
        let popper = new Popper($('.first-calc-slider .tootip'), onPopper, {
            placement: 'top',
        });
    });
    const priceSlider = ()=>{
        let min = 2100, max = 15000, value = 4500, step = 100
        $('.first-calc-slider').stop().slider({
            range: "min", value, min, max, step,
            slide: (event, ui)=> {
                $(ui.handle).find('.tooltip').text(convert.currency(ui.value));
                reset2nd(ui);
            },
            create: (event, ui)=> {
                let tooltip = $('<div class="tooltip" />').text(convert.currency(value));
                let lableMin = $('<div class="slider-lable-min slider-lable" />').text(convert.currency(min));
                let lableMax = $('<div class="slider-lable-max slider-lable" />').text(convert.currency(max));
                $(event.target).find('.ui-slider-handle').append(tooltip);
                $(event.target).append(lableMin);
                $(event.target).append(lableMax);
                
                
            },
            change: (event, ui)=> {
                $('#hidden').attr('value', ui.value);
                changeAountText();
            }
        });
    }
    const durationSlider = ()=>{
        let min = 3, max = 24, value = 12, step = 1, minL = '3 Months', maxL =  '2 Years'
        $('.secound-calc-slider').stop().slider({
            range: "min",
            value,
            min,
            max,
            step,
            slide: (event, ui)=> {
                $(ui.handle).find('.tooltip').text(convert.months(ui.value));
            },
            create: (event, ui)=> {
                let tooltip = $('<div class="tooltip" />').text(convert.months(value));
                let lableMin = $('<div class="slider-lable-min slider-lable" />').text(minL);
                let lableMax = $('<div class="slider-lable-max slider-lable" />').text(maxL);
                $(event.target).find('.ui-slider-handle').append(tooltip);
                $(event.target).append(lableMin);
                $(event.target).append(lableMax);
            },
            change: (event, ui)=> {
                $('#hidden').attr('value', ui.value);
                changeAountText();
            }
        });
    }
    const rateSlider = ()=>{
        let min = 1, max = 6, value = 6, step = 1, minL = 'Not Bad', maxL =  'Great';
        $('.third-calc-slider').stop().slider({
            range: "min",
            value,
            min,
            max,
            step,
            slide: (event, ui)=> {
                $(ui.handle).find('.tooltip').text(ui.value);
                let caseValue = ui.value || 6, stringVal;
                switch (caseValue) {
                    case 1:
                        stringVal = '29.99%';
                        break;
                    case 2:
                        stringVal = '28.00%';
                        break;
                    case 3:
                        stringVal = '22.00%';
                        break;
                    case 4:
                        stringVal = '15.00%';
                        break;
                    case 5:
                        stringVal = '11.00%';
                        break;
                    case 6:
                        stringVal = '8.99%';
                        break;
                
                    default:
                        stringVal = '8.99%';
                        break;
                }
                $('.third-calc-slider').find('.tooltip').text(stringVal);
            },
            create: (event, ui)=> {
                let tooltip = $('<div class="tooltip" />').text('8.99%');
                let lableMin = $('<div class="slider-lable-min slider-lable" />').text(minL);
                let lableMax = $('<div class="slider-lable-max slider-lable" />').text(maxL);
                $(event.target).find('.ui-slider-handle').append(tooltip);
                $(event.target).append(lableMin);
                $(event.target).append(lableMax);
            },
            change: (event, ui)=> {
                $('#hidden').attr('value', ui.value);
                changeAountText();
            }
        });
    }
    const convert = {
        currency : x => '$ '+x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","),
        months : x => x+' months',
    }
    const reset2nd = ui=>{
        let slider2nd = $(".secound-calc-slider");
        let oldValue = slider2nd.slider('option', 'value');
        if(ui.value <= 3000){
            slider2nd.slider({value: 3});
        }
        if(ui.value > 5000){
            slider2nd.slider({min: 12, max: 36});
            $(".secound-calc-slider .slider-lable-min").text('1 Years');
            $(".secound-calc-slider .slider-lable-max").text('3 Years');
            if(oldValue < 12 || oldValue > 36 ){
                slider2nd.slider({value: 12});
            }  
        }
        else if(ui.value > 4600){
            slider2nd.slider({min: 12, max: 24});
            $(".secound-calc-slider .slider-lable-min").text('1 Years');
            $(".secound-calc-slider .slider-lable-max").text('2 Years');
            if(oldValue < 12 || oldValue > 24 ){
                slider2nd.slider({value: 12});
            }
        }
        else{
            slider2nd.slider({min: 3, max: 24});
            $(".secound-calc-slider .slider-lable-min").text('3 Months');
            $(".secound-calc-slider .slider-lable-max").text('2 Years');
            if(oldValue < 3 || oldValue > 24 ){
                slider2nd.slider({value: 3});
            }
        }

        if(ui.value < 4500){
            if(1){
                
                slider2nd.slider({min: 3, max: 24});
                $(".secound-calc-slider .slider-lable-min").text('3 Months');
                $(".secound-calc-slider .slider-lable-max").text('2 Years');
            }
        }else if(ui.value < 5000){
            slider2nd.slider({min: 12, max: 36});
            $(".secound-calc-slider .slider-lable-min").text('1 Years');
            $(".secound-calc-slider .slider-lable-max").text('3 Years');
            if(oldValue < 12)
                slider2nd.slider({value: 12});
        }else{
            slider2nd.slider({min: 12, max: 36});
            $(".secound-calc-slider .slider-lable-min").text('1 Years');
            $(".secound-calc-slider .slider-lable-max").text('3 Years');
            if(oldValue < 12)
                slider2nd.slider({value: 12});
        }
        slider2nd.find('.tooltip').text(convert.months(slider2nd.slider("option", 'value')));
    }
    const changeAountText = ()=>{
        let p = $(".first-calc-slider").slider("option", "value"),
            r = parseFloat($('.third-calc-slider .tooltip').text()),//$(".third-calc-slider").slider("option", "value"),
            t = $(".secound-calc-slider").slider("option", "value");
            setAmount(p,r,t);
    }
    const setAmount = (p,r,t)=>{
        $('.monthly-amt').text(convert.currency((totalAmount(p,r,t) / t).toFixed(2)));
        $('.total-amt').text(convert.currency((totalAmount(p,r,t)).toFixed(2)));
    }
    const simpleIntrest = (p,r,t)=>(p*r*(t/12))/100;
    var intr = r=> r/ 1200;
    const loanIntrest = (p,r,t)=> p * intr(r) / (1 - (Math.pow(1/(1 + intr(r)), t)));;
    const totalAmount = (p,r,t)=>loanIntrest(p,r,t) * t;
})();